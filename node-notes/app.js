const fs = require("fs");
const _ = require("lodash");
const yargs = require("yargs");

const notes = require("./notes.js");

var titleDetails = {
	describe: "Title of note",
	demand: true,
	alias: "t"
};
var bodyDetails = {
	describe: "Body of note",
	demand: true,
	alias: "b"
};
var argv = yargs
	.command("add", "add a new note", {
		title: titleDetails,
		body: bodyDetails
	})
	.command("list", "Listing all notes")
	.command("read", "Read a note", {
		title: titleDetails
	})
	.command("remove", "remove a note", {
		title: titleDetails
	})
	.help().argv;

var command = argv._[0];

if (command === "add") {
	var note = notes.addNote(argv.title, argv.body);
	if (note) {
		console.log("note created");
		notes.logNote(note);
	} else console.log("title with name " + argv.title + " already exists");
} else if (command === "list") {
	var allNotes = notes.getAll();
	console.log(`Printing ${allNotes.length} note(s)`);
	allNotes.forEach(note => notes.logNote(note));
} else if (command === "read") {
	var note = notes.readNote(argv.title);
	if (note) {
		console.log("note contains");
		notes.logNote(note);
	} else console.log("note not found");
} else if (command === "remove") {
	var noteRemoved = notes.removeNote(argv.title);
	var message = noteRemoved ? "note removed" : "note not found";
	console.log(message);
} else {
	console.log("Command invalid");
}
