const request = require("request");

var getWeather = (lat,lng) => {

    return new Promise((resolve,reject) => {
    request(
        {
            url:
                `https://api.darksky.net/forecast/ebacabc1a1b620fe9381896ab6465119/${lat},${lng}`,
            json: true
        },
        (error, response, body) => {
            if (error) {
                reject("Unable to connect to forecast.io");
            } else if (response.statusCode === 404) {
                reject("Unable to fetch temperature");
            } else if (response.statusCode === 200)
                resolve({
                   temperature: body.currently.temperature,
                   apparentTemperature  : body.currently.apparentTemperature
                });
        }
    );
});
};

module.exports.getWeather = getWeather;
