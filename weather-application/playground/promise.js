var asyncAdd = (a, b) => {
	return new Promise((resolve, reject) => {
		if(typeof a === 'number' && typeof b === 'number'){
			resolve(a+b);
		}else{
			reject('Arguments should be numbers');
		}
	});
};

asyncAdd(11, 45).then ((res) => {
	console.log('Result : ',res);
	return asyncAdd(res, '10');
}).then((result) => {
	console.log('Result should be 66', result);
}).catch((errorMessage) => {
	console.log(errorMessage);
})




// var somePromise = new Promise((resolve, reject) => {
// 	setTimeout(() => {
// 		resolve("hey! it's worked")
// 		reject('Unable to resolve promise')
// 	},2500)
// });

// somePromise.then((message) => {
// 	console.log('Success : ',message);
// },(errorMessage) => {
// 	console.log('Error : ',errorMessage);
// })