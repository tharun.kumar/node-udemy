const yargs = require("yargs");

const geocode = require("./geocode/geocode.js");
const weather = require("./weather/weather.js");

const argv = yargs
    .options({
        address: {
            describe: "Adress to get weather for",
            demand: true,
            alias: "a",
            string: true
        }
    })
    .help()
    .alias("help", "h").argv;

geocode.geocodeAddress(argv.address, (errorMessage, results) => {
    if (errorMessage) {
        console.log(errorMessage);
    } else {
        console.log(results.address);
        // weather.getWeather(results.latitude, results.longitude, (errorMessage, results) => {
        //     if (errorMessage) {
        //         console.log(errorMessage);
        //     } else {
        //         console.log(`It's currently ${results.temperature}. It's feeling like ${results.apparentTemperature}`);
        //     }
        // });

        weather.getWeather(results.latitude, results.longitude).then((results)=>{
            console.log(`It's currently ${results.temperature}. It's feeling like ${results.apparentTemperature}`);
        },(errorMessage)=>{
            console.log(errorMessage);
        })


    }
});
